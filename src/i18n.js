import i18n from "i18next"
import { initReactI18next } from "react-i18next"
import LanguageDetector from 'i18next-browser-languagedetector'






i18n
.use(initReactI18next) 
.use (LanguageDetector)// passes i18n down to react-i18next
  .init({
    resources: {
      en: {
        translation: {
          welcome: {
            "text": 'Welcome to this React Internationalization App',
          },
        },
      },
      kr: {
        translation: {
          welcome: {
            "text": "이 React 국제화 앱에 오신 것을 환영합니다",
          },
        },
      },
      kh: {
        translation: {
          welcome: {
            "text": "សូមស្វាគមន៍មកកាន់កម្មវិធី React Internationalization App នេះ",
          },
        },
      },
    },
    
  });