import { BrowserRouter as BigRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import HomePage from './pages/HomePage';
import CustomNavBar from './components/CustomNavBar';
import NotFoundPage from './pages/NotFoundPage';
import Service from './pages/Service';
import AboutUs from './pages/AboutUs';
import MakeUp from './pages/Make-up';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import Products from './pages/Products';
import ProductDetail from './pages/ProductDetail';
import ProductDashBoard from './pages/admin/ProductDashBoard';
import Dashboard from './pages/admin/Dashboard';
import UserDashboard from './pages/admin/UserDashboard';
import { useContext, useEffect, useState } from 'react';
import Layout from './components/routes/Layout';
import { ToastContainer } from 'react-toastify';
import AuthContext, { AuthContextProvider } from './components/shared/AuthContext';
import { GET_OWN_PROFILE } from './services/userService';
import { useTranslation } from 'react-i18next';
import i18n from "i18next";



function App() {
  const lngs = [
    {
      code: "en",
      nativeName: "English"
    },
    {
      code: "kh",
      nativeName: "Khmer"
    },
    {
      code: "kr",
      nativeName: "Korea"
    }
  ];

  const { t } = useTranslation();

  const [isValidated, setIsValidated] = useState(localStorage.getItem("token")? true : false);

  useEffect(() => {
    if (localStorage.getItem("token")) {
      GET_OWN_PROFILE().then((response) => {
        console.log("This is the response of the user profile:", response);
        setIsValidated(true);
      }).catch((error) => {
        console.error("Error fetching user profile:", error);
        setIsValidated(false);
      });
    }
  }, []);

  const handleLoginCallBack = (value) => {
    setIsValidated(value); // Assuming this gets called after successful login
  };

  return (
    <>
      <div className='container'>
        <h1 className='text-center'>{t('welcome.text')}</h1>
        <div className='text-center'>
          {
            lngs.map((lng) => {
              return (
                <button className='btn btn-warning ms-2' key={lng.code} type='submit' onClick={() => i18n.changeLanguage(lng.code)}>
                  {lng.nativeName}
                </button>
              );
            })
          }
        </div>

        <BigRouter>
          <AuthContextProvider>
            <CustomNavBar isAuthenticated={isValidated} loginCallBack={handleLoginCallBack} />
            <Routes>
              {!isValidated?
                <>
                  <Route index element={<Layout includeFooter={true}><HomePage /></Layout>} />
                  <Route path="/service" element={<Layout includeFooter={true}><Service /></Layout>} />
                  <Route path="/makeup" element={<Layout includeFooter={true}><MakeUp /></Layout>} />
                  <Route path="/aboutus" element={<Layout includeFooter={true}><AboutUs /></Layout>} />
                  <Route path="/products" element={<Products />} />
                  <Route path="/products/:id" element={<ProductDetail />} />
                </>
                :
                <>
                  <Route path='/admin/products' element={<ProductDashBoard />} />
                  <Route index element={<Dashboard />} />
                  <Route path='/admin/users' element={<UserDashboard />} />
                </>
              }
              <Route path="*" element={<NotFoundPage />} />
            </Routes>
            <ToastContainer />
          </AuthContextProvider>
        </BigRouter>
      </div>
    </>
  );
}

export default App;