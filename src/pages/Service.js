import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import '../styles/serviceStyle.css'

const Service = () => {

    return (
        <div className="d-flex flex-column service">
            <div
                className="container d-flex 
        gap-2
      flex-column flex-md-row justify-content-between mt-5"
            >
                <div className="text-side w-100 w-md-50 order-2 order-md-1">
                    <h1> Skincare is self-care</h1>

                    <p> Taking care of your skin isn't just about appearance; it's about nurturing your body and mind. It's a moment of mindfulness and relaxation that allows you to connect with yourself and prioritize your well-being. </p>

                    <button  style={{ backgroundColor: '#F11A7B', color: '#F9F5F6' }} className="btn btn-success mt-5">
                        {" "}
                        <span className="pre-scrollable  ">
                            <strong> Discover More  </strong>
                        </span>
                        <FontAwesomeIcon icon={faArrowRight} />
                    </button>
                </div>

                <div className="image-side order-1 order-md-3 text-center w-100 w-md-75 m-lg-5 p-lg-5   my-md-auto my-lg-0 ">
                    <img
                        style={{width:'150px', height:'500px'}}
                        className="img-fluid rounded-3 w-100"
                        src="https://i.pinimg.com/736x/f9/a3/6f/f9a36ff4b6b52e1062d40664c39a23d2.jpg"
                        alt=" image for the service "
                    />
                </div>
            </div>

            <div className="two-feature mb-5 mt-4  container  d-flex flex-column flex-md-row  gap-3">
                <div className="pop-card bg-success w-100 w-md-50 rounded-3 mt-3  px-5 py-5">
                    <h3 className="">New Arrivals</h3>
                    <h2> Essence </h2>
                    <p className="mt-3">
                        <b> Price </b>
                        <br />
                        <strong> $ 20.00 </strong>
                    </p>
                </div>

                <div className="pop-card1 bg-success w-100 w-md-50 rounded-3 mt-3 px-5 py-5">
                    <h3 className="">New Arrivals</h3>
                    <h2> Night Cream </h2>
                    <p className="mt-5">
                        <b> Price after discount </b>
                        <br />
                        <strong> $ 25.06 </strong>
                    </p>
                </div>
            </div>

            <div className="section2 mt-5 container">
                <div
                    className="service-provide mt-5 
        mb-4 text-center"
                >
                    <h2> Our Glow Skincare  </h2>
                </div>

                <div className="feature-service d-flex   flex-column gap-5 gap-md-1  flex-md-row   mt-4">
                    <div className="d-flex gap-2 flex-column flex-lg-row">
                        <div className="service-card   w-100 w-md-25 w-lg-25     text-center mx-3 px-2">
                            <img
                            style={{width:'270px', height:'280px'}}
                                effect="opacity"
                                className="img-fluid "
                                src="https://static.wixstatic.com/media/92ef81_42dafb1374194ab18fe80f8bb47e270b~mv2.jpg/v1/fit/w_2500,h_1330,al_c/92ef81_42dafb1374194ab18fe80f8bb47e270b~mv2.jpg"
                                alt=""
                            />

                            {/* <img
                className="img-fluid w-75"
                src="https://img.icons8.com/?size=512&id=vGFJhv8oyS5d&format=png"
                alt=""
              /> */}
                            <h4>Signature Serum</h4>
                            <p>Introducing our revitalizing serum for radiant skin! Discover the power of our serum to hydrate, smooth, and rejuvenate your complexion. Unlock your skin's natural glow with our advanced formula.</p>
                        </div>
                        <div className="service-card  text-center w-100 w-md-50 w-lg-25    mx-3 px-2">
                            <img
                            style={{width:'270px', height:'280px'}}
                                effect="opacity"
                                className="img-fluid "
                                src="https://static.wixstatic.com/media/92ef81_bc66ea5fc6574fe186ba668ed89711d6~mv2.jpg/v1/fill/w_980,h_980,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/92ef81_bc66ea5fc6574fe186ba668ed89711d6~mv2.jpg"
                                alt=""
                            />
                            {/* <img
                className="img-fluid w-75"
                src="https://img.icons8.com/?size=512&id=7pyBnIFIx5OY&format=png"
                alt=""
              /> */}
                            <h4>Barrier Moisturizer</h4>
                            <p>Experience deep hydration and nourishment with our Moisturizer. It's formulated with potent ingredients to keep your skin hydrated and healthy. Discover the secret to a radiant complexion with our Moisturizer!</p>
                        </div>
                    </div>

                    <div className="d-flex gap-2 flex-column flex-lg-row">
                        <div className="service-card  text-center w-100 w-md-50 w-lg-25    mx-3 px-2">
                            {/* <img
                className="img-fluid w-75"
                src="https://img.icons8.com/?size=512&id=SpZSUswN9tJs&format=png"
                alt=""
              /> */}

                            <img
                          style={{width:'270px', height:'280px'}}
                                effect="opcaity"
                                className="img-fluid "
                                src="https://static.wixstatic.com/media/92ef81_fb9a38fea63b49c48a7e05d79be0dc6c~mv2.jpg/v1/fill/w_980,h_980,al_c,q_85,usm_0.66_1.00_0.01,enc_auto/92ef81_fb9a38fea63b49c48a7e05d79be0dc6c~mv2.jpg"
                                alt=""
                            />
                            <h4>Cleasing Balm</h4>
                            <p>Dive into a luxurious cleansing experience with our gentle Cleasing Balm. It effectively removes impurities while leaving your skin feeling refreshed and revitalized. Elevate your skincare routine with our Foaming Cleanser for a clear and radiant complexion!</p>
                        </div>
                        <div className="service-card w-25  text-center mx-3 px-2 w-100 w-md-50 w-lg-25   ">
                            {/* <img
                className="img-fluid w-75"
                src="https://img.icons8.com/?size=512&id=rMYPr6ODJKc5&format=png"
                alt=""
              /> */}
                            <img
                            style={{width:'270px', height:'280px'}}
                                effect="opacity"
                                className="img-fluid  "
                                src="https://besoma.artbizdigital.com/uploads/DSC_05536_Edit_1_1_630c4a6bea.jpg"
                                alt=""
                            />
                            <h4>Glow Mask</h4>
                            <p>Treat your skin to the ultimate pampering session with our Face Mask. Infused with nourishing botanicals and vitamins, our masks rejuvenate and revitalize your skin, leaving it soft, smooth, and radiant. Elevate your skincare routine with our luxurious Face Mask today!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Service;
