
import React from 'react';

import '../styles/makeUp.css'; 

const MakeUp = () => {
  return (
    <div className="makeup-page">
      <h1 className="page-title"> Best Seller Cosmetic Products</h1>
      <div className="product-grid">
        <div className="product-card">
          <img src="https://hips.hearstapps.com/hmg-prod/images/elm050123btycouturelipstick-001a-646290e1499b5.jpg" alt="Lipstick" className="product-image" />
          <h2 className="product-name">Red Lipstick</h2>
          <p className="product-description">A classic red lipstick perfect for everyday wear.</p>
        </div>
        <div className="product-card">
          <img src="https://www.temptalia.com/wp-content/uploads/2019/06/dior_amber-neutrals-003_001_palette.jpg" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Neutral Eyeshadow Palette</h2>
          <p className="product-description">A versatile palette for creating various eye looks.</p>
        </div>
        <div className="product-card">
          <img src="https://johnlewis.scene7.com/is/image/JohnLewis/236240826" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Contour Duo Stick</h2>
          <p className="product-description">Dual-ended sculpting stick for highlighting and contouring.</p>
        </div>
        <div className="product-card">
          <img src="https://www.dior.com/dw/image/v2/BGXS_PRD/on/demandware.static/-/Sites-master_dior/default/dwdb33772d/Y0803050/Y0803050_F080305094_E01_GHC.jpg?sw=1850&sh=1850" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Eyeliner Waterproof</h2>
          <p className="product-description">Define dramatic eyes with this long-lasting, smudge-proof eyeliner.</p>
        </div>
        <div className="product-card">
          <img src="https://shop-beauty.dior.ae/cdn/shop/files/rougedior720_1024x1024@2x.jpg?v=1711543734" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">ROUGE BLUSH</h2>
          <p className="product-description">Adds natural-looking flush of color to cheeks.</p>
        </div>
        <div className="product-card">
          <img src="https://www.sephora.com/productimages/sku/s2509651-main-zoom.jpg?imwidth=315" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">BACKSTAGE Concealer</h2>
          <p className="product-description">Flawless coverage, behind-the-scenes secret weapon.</p>
        </div>
        <div className="product-card">
          <img src="https://shop-beauty.dior.ae/cdn/shop/products/Y0264250_C026425090_E01_GHC_1850x.jpg?v=1694793393" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">24H VOLUME MASCARA</h2>
          <p className="product-description">Curls, lengthens, and defines lashes for a dramatic look.</p>
        </div>
        <div className="product-card">
          <img src="https://goldengirlcosmetics.com/cdn/shop/products/Colorox-Hair-Color-New-1bbd8e2-goldengirl.jpg?v=1709801105" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Cream Hair Color</h2>
          <p className="product-description">Easy-use cream formula deposits vibrant, natural tones. </p>
        </div>
        <div className="product-card">
          <img src="https://m.media-amazon.com/images/I/31ihxVVYGtL.jpg" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Eyebrow Pencil</h2>
          <p className="product-description">Defines brows for a natural or dramatic look.</p>
        </div>
        <div className="product-card">
          <img src="https://www.dior.com/dw/image/v2/BGXS_PRD/on/demandware.static/-/Sites-master_dior/default/dwb9b133cd/Y0023000/Y0023000_C002300001_E01_GHC.jpg?sw=800" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Powders</h2>
          <p className="product-description">Fine, dry powders billowed in clouds from the overflowing sacks.</p>
        </div>
        <div className="product-card">
          <img src="https://www.dior.com/dw/image/v2/BGXS_PRD/on/demandware.static/-/Sites-master_dior/default/dw8759a38c/Y0124000/Y0124000_C012400001_E01_GHC.jpg?sw=800" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Dior Lip Glow Oil</h2>
          <p className="product-description">Dior Lip Glow Oil nourishes lips with a natural shine.</p>
        </div>
        <div className="product-card">
          <img src="https://www.sephora.com/productimages/sku/s2244440-main-zoom.jpg" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">BACKSTAGE Lash Curler</h2>
          <p className="product-description">Creates dramatic, long-lasting curled lashes.</p>
        </div>
        <div className="product-card">
          <img src="https://i5.walmartimages.com/seo/Dior-Grand-Bal-False-Lashes_b5a9943c-cd1f-4181-95df-0333b037624d.284ad8d68c1fd580c06eddc7e29993d2.jpeg" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Grand Bal Lashes</h2>
          <p className="product-description">Lengthen and volumize lashes for dramatic eyes.</p>
        </div>
        <div className="product-card">
          <img src="https://shop-beauty.dior.ae/cdn/shop/products/Y0293000_C029300010_E01_GHC_1850x.jpg?v=1694793481" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">VELVET COMPACT FOUNDATION</h2>
          <p className="product-description">Lightweight makeup creates flawless, even-toned canvas.</p>
        </div>
        <div className="product-card">
          <img src="https://image.harrods.com/dior-dior-backstage-eyeshadow-palette_19165994_48783198_2048.jpg" alt="Eyeshadow" className="product-image" />
          <h2 className="product-name">Backstage Eyeshadow Palette</h2>
          <p className="product-description">Creamy formula for effortless, crease-proof application. </p>
        </div>
        
        
        {}
      </div>
    </div>
  );
};

export default MakeUp;
