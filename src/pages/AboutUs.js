import React from "react";
import "../styles/aboutUsStyle.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoneyBillTransfer, faPlay } from "@fortawesome/free-solid-svg-icons";
import {
    faFacebook,
    faInstagram,
    faLinkedin,
    faTwitter,
} from "@fortawesome/free-brands-svg-icons";

const AboutUs = () => {


    return (
        <div className="container aboutus">
            <div className="section1  mt-5 d-flex   flex-md-row align-items-md-center gap-md-4 justify-content-center flex-column  ">
                <div className="image-side w-sm-100 w-md-50    w-lg-60 text-center ">




                </div>
                
            </div>

            <div className="section2   mt-5 pt-5">
                <div className="">
                    <h1 className="text-center"> Why should you choose our products?</h1>
                    <p className="text-center px-5">
                    Discover the beauty of nature with Skiny! We use natural ingredients carefully selected to nourish and revitalize your skin.
                    </p>
                </div>

                <div className="d-sm-flex flex-sm-column flex-md-row justify-content-center mt-5 gap-5">
                    <div className="feature-card  w-md-25 w-lg-25 w-sm-100  d-flex flex-column align-items-center ">
                        <img
                         style={{width:'500px', height:'350px'}}
                            className="img-fluid "
                            src="https://cdn-icons-png.flaticon.com/512/6152/6152120.png"
                            alt=""
                        />
                        <h4 className="mt-3">  ✨Effective Formulas</h4>
                        <p className="text-center">
                        Our skincare products target specific skin concerns with effective formulations. Experience visible results and radiant, healthy skin.
                        </p>
                    </div>
                    <div className="feature-card  w-md-25 w-lg-25   d-flex flex-column  align-items-center">
                        <img
                         style={{width:'500px', height:'350px'}}
                            className="img-fluid"
                            src="https://cdn-icons-png.flaticon.com/512/10572/10572500.png"
                            alt=""
                        />
                        <h4 className="mt-3 text-center"> 🌊Hydration and Balance </h4>
                        <p className="text-center">
                        Achieve skin balance with our hydrating formulas. Maintain natural moisture and protect against daily stressors.




                        </p>
                    </div>
                    <div className="feature-card w-md-25 w-lg-25 w-sm-100 d-flex flex-column align-items-center">
                        <img
                          style={{width:'500px', height:'350px'}}
                            className="img-fluid"
                            src="https://i.pinimg.com/originals/c4/c0/a5/c4c0a53e65b6d16061e2efcd5a6c193c.png"
                            alt=""
                        />
                        <h4 className="mt-3 text-center">
                        🌸Skin-Friendly Innovations
                        </h4>
                        <p className="text-center">
                            
Elevate your skincare journey with Skiny. Embrace pure beauty with us! Unleash Your Radiance! 
                        </p>
                    </div>
                </div>
            </div>

            <div className="section3 d-flex-column  ">
                <div
              
                    className=" w-md-25 w-lg-25
        mt-5
        d-flex 
        flex-column
        flex-md-row  
        align-items-center 
        justify-content-around
       
       
        "
                >
                    <div className="img-side">
                        <img
                            src="https://cdn3.iconfinder.com/data/icons/finance-81/64/Hand_shake_agreement_business_commitment_meeting-512.png"
                            alt=""
                            className="img-fluid w-100"
                        />
                    </div>
                    <div className=" col-sm-12 col-md-6    ">
                        <h1>  Our Commitment </h1>
                        <ol>
            <li>Natural Ingredients: Skiny emphasizes the use of natural and botanical ingredients in their products, promoting skincare solutions that harness the power of nature.</li>
            <li>Cruelty-Free Practices: Many customers value brands that do not test their products on animals. Skiny, like several other skincare brands, has expressed a commitment to cruelty-free practices.</li>
            <li>Environmental Responsibility: Some beauty brands, including Nature Republic, are increasingly focusing on environmentally friendly practices. This may include sustainable sourcing of ingredients, eco-friendly packaging, and reducing the environmental impact of their operations.</li>
            <li>Transparency: Skincare brands often commit to transparency in their product formulations. Skiny may provide customers with detailed information about the ingredients used, allowing them to make informed choices.
    
            </li>
            <li>Customer Education: Beauty brands, including Skiny, may be committed to educating customers about skincare routines, product usage, and the benefits of specific ingredients. This could be through online resources, product packaging, or customer support.</li>
            
          </ol>

                        <button style={{ backgroundColor: '#F11A7B', color: '#F9F5F6' }} className="btn btn-success mt-3">
                            <span className="pre-scrollable ">
                                Support Us </span>
                            <FontAwesomeIcon icon={faMoneyBillTransfer} />
                        </button>
                    </div>
                </div>

                
            </div>
        </div>
    );
};

export default AboutUs;
