import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faPaperPlane } from "@fortawesome/free-solid-svg-icons";

import "../styles/homepageStyle.css";

const HomePage = () => {

    return (
        <div className="container">
            <div className="d-flex header-section flex-column flex-md-row  mt-4 ">
            <div className="container pt-4   order-2 order-md-1">
                    <h1 style={{ color: '#3E001F'}} className="display-2 header-title">
                        {" "}
                        Unlock Your Skin's Potential
                    </h1>
                    <p> Regular skincare can contribute to overall well-being by boosting confidence and promoting self-care. Understanding your skin type and using appropriate products is key to effective skincare.</p>
                    <div className="d-flex pt-5">
                        <button style={{ backgroundColor: '#F11A7B', color: '#F9F5F6' }} className="btn btn-success px-4 py-2">
                            {" "}
                            <strong> Shop Now </strong>{" "}
                        </button>
                        <button className="ps-4 nav-link">
                            <span>
                                <strong className="pre-scrollable">
                                    View More {"  "}
                                </strong>
                            </span>
                            <FontAwesomeIcon icon={faArrowRight} />
                        </button>
                    </div>
                </div>

                <div className="order-md-2">
                    <img
                        className="img-fluid "
                        src="https://static.thcdn.com/images/large/webp//productimg/1600/1600/15061286-1195140589108706.jpg "
                        alt="image logo "
                    />
                </div>
            </div>

            <div className="container text-center mt-5 pt-5 mb-5">
                <h1 style={{  color: '#EA1179' }} className="feature-title">
                    {" "}
                    Skin First , 
                     
                    <span style={{  color: '#3E001F' }} className="feature-title  pre-scrollable">
                        Make-Up second
                    </span>
                </h1>
                <div className="d-flex flex-column container flex-wrap flex-md-row justify-content-center gap-5 mt-5 ">
                    <div className="item-card px-4 pb-3  col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid p-4"
                            src="https://aesopskincare.ph/cdn/shop/products/Aesop-Skin-Lightweight-Facial-Hydrating-Serum-100mL-Large-835x962px_-_ASK20RF.png?v=1602191354"
                            alt=" plant image "
                        />
                        <h2> WHITE TEA SERUM</h2>
                        <p> Treat yourself to the luxury of nature.</p>
                    </div>
                    <div className="item-card px-4 pb-3 col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid"
                            src="https://sdcdn.io/mac/us/mac_sku_NY9NJ0_1x1_0.png?width=1440&height=1440"
                            alt=" plant image "
                        />
                        <h2> SILKY MATTE LIPSTICK</h2>
                        <p> Enhance your look with vibrant colors and nourishing formulas for irresistible lips.</p>
                    </div>
                    <div className="item-card px-4 pb-3 col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid "
                            src="https://www.bareminerals.com/cdn/shop/files/BM_SP24_BPLF_Fair15Cool_SILO_TEXTURE_3000x3000_R150.png?v=1706874472&width=500"
                            alt=" plant image "
                        />
                        <h2> MATTE LIQUID FOUNDATION</h2>
                        <p>  Achieve a radiant complexion with our range of shades for every skin tone.</p>
                    </div>
                </div>
            </div>

            <div
                className="container section3 d-flex flex-column 
      flex-md-row mt-5 pt-5 justify-content-center align-items-center"
            >
                <div className="image-side  col-sm-1 col-md-1 col-lg-4  ">
                    <img
                    style={{width: '500px', height: '500px', marginRight:'200px'}}
                        className="img-fluid  "
                        src="https://i.pinimg.com/736x/fe/e9/e9/fee9e9f84adde0350fdbcf76c987236b.jpg"
                        alt="various skincare"
                    />
                </div>

                <div style={{ color: '#3E001F' }} className="text-side ">
                    <h1  className="feature-title mx-5">Why people engage in skincare?</h1>
                    <ul>
                        <li>Skin shields you from germs and environmental damage like sunlight. Skincare helps keep this barrier strong by removing dirt and keeping it hydrated.</li>
                        <li> Regular care can help prevent issues like acne or dryness that can lead to infection or discomfort.</li>
                        <li> Sun exposure and natural aging take a toll on skin. Skincare can help slow the process and minimize wrinkles.</li>
                    </ul>
                </div>
            </div>

            
        </div>
    );
};

export default HomePage;
