import React from 'react'
import '../../styles/userdash.css'

const UserDashboard = () => {
    return (
        <div className="dashboard-container">
      <div className="sidebar">
        <h2>Menu</h2>
        <ul>
          <li>Home</li>
          <li>Profile</li>
          <li>Settings</li>
          <li>Logout</li>
        </ul>
      </div>
      <div className="main-content">
        <h1>Welcome to Your Dashboard</h1>
        <p>This is your personalized dashboard.</p>
      </div>
    </div>
    )
}

export default UserDashboard