import { useContext, useState } from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { NavLink, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import UserSignUpModal from './UserSignUpModal';
import UserSignInModal from './UserSignInModal';
import AuthContext from './shared/AuthContext';

function CustomNavBar({ isAuthenticated, loginCallBack }) {
    const { logout, user } = useContext(AuthContext);
    const navigate = useNavigate();
    const [showSignUp, setShowSignUp] = useState(false);
    const [showSignIn, setShowSignIn] = useState(false);

    // New state to track if the user is logged in
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const ClientLinks = () => (
        <>
            <NavLink className={"nav-link"} to="/">
                Home
            </NavLink>
            <NavLink className={"nav-link"} to="/service">
                Skin Care
            </NavLink>
            <NavLink className={"nav-link"} to="/makeup">
                Make-Up
            </NavLink>
            <NavLink className={"nav-link"} to="/products">
                Products
            </NavLink>
            <NavLink className={"nav-link"} to="/aboutus">
                About Us
            </NavLink>
        </>
    );

    const AdminLinks = () => (
        <>
            <NavLink className={"nav-link"} to="/">
                Dashboard
            </NavLink>
            <NavLink className={"nav-link"} to="/admin/products">
                Product Dashboard
            </NavLink>
            <NavLink className={"nav-link"} to="/admin/users">
                User Dashboard
            </NavLink>
        </>
    );

    const handleLogin = () => {
        setShowSignIn(true);
        loginCallBack(true); // Ensure the callback is called
        setIsLoggedIn(true); // Update the isLoggedIn state
        navigate("/"); // Redirect to the home page
    };

    const handleSignUp = () => {
        setShowSignUp(true);
    };

    const handleCloseSignUpModal = () => {
        setShowSignUp(false);
    };

    const handleCloseSignInModal = () => {
        setShowSignIn(false);
    };

    const handleLogout = () => {
        logout();
        setIsLoggedIn(false); // Reset isLoggedIn state upon logout
    };

    return (
        <Navbar expand="lg" className="bg-body-tertiary sticky-top">
            <UserSignUpModal showSignUp={showSignUp} handleCloseSignUp={handleCloseSignUpModal} />
            <UserSignInModal showSignIn={showSignIn} handleCloseSignIn={handleCloseSignInModal} />
            <Container>
                <Navbar.Brand href="#home" style={{ fontFamily: 'Cursive', fontSize: '30px' }}>Skiny</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mx-auto text-center">
                        {isAuthenticated? <AdminLinks /> : <ClientLinks />}
                    </Nav>
                    <div className="d-flex justify-content-center">
                        {!isAuthenticated? (
                            <>
                                <button
                                    className='btn btn-primary me-3'
                                    style={{ backgroundColor: '#F11A7B', color: '#F9F5F6' }}
                                    onClick={handleSignUp}> Sign Up
                                </button>
                                <button
                                    className='btn btn-primary'
                                    style={{ backgroundColor: '#3E001F', color: '#F9F5F6' }}
                                    onClick={handleLogin}>
                                    Login
                                </button>
                            </>
                        ) : (
                            <button
                                style={{ backgroundColor: '#F11A7B', color: '#F9F5F6' }}
                                className='btn btn-warning'
                                onClick={handleLogout}
                            >
                                Log Out
                            </button>
                        )}
                    </div>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default CustomNavBar;
