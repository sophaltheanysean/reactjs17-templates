import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import {
  faFacebook,
  faInstagram,
  faTwitter,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
 
 
// import facebook
import "../styles/footerStyle.css";
const Footer = () => {

  
  return (
    <div className="footer ">
      <div className="d-sm-flex flex-sm-column 
      flex-lg-row    
      gap-4 
      px-5 pt-4">
        <div className="left-sides d-sm-flex flex-sm-column   flex-sm-column flex-md-row align-items-md-center  ">
         
          <img
            className="d-block "
            src="https://cdn-icons-png.flaticon.com/512/6075/6075201.png"
            alt="website logo "
          />

          <div className="mt-4  ">
            <h4 className="logo-name  " style={{fontFamily: 'Cursive', fontSize:'35px' , color: '#F11A7B' }}>Skiny</h4>
            <p className="col-lg-7">
            Elevate your beauty routine with our curated skincare essentials. Discover radiance in every product. Unleash the glow within and embrace a healthier, more vibrant complexion. Shop now for radiant skin!
            </p>
          </div>
        </div>

        <div className="middle-side">
          {/* <div className="d-flex justify-content-around">
            <a className="nav-link"  href="#"> Home </a>
            <a className="nav-link" href="#"> Service </a>
            <a className="nav-link" href="#"> About Us</a>
             
          </div> */}
        </div>

        <div className="right-side   d-md-flex flex-sm-column flex-md-row align-items-md-start justify-content-md-between gap-md-2 flex-lg-column  ">
          <div className="">
            <h4 style={{ color: '#3E001F'  }}> Email Us </h4>
            <p className="w-md-100">
              {" "}
              Here is a concise version of your request.
            </p>
            <div className="d-flex">
              <input type="text" />
              <button style={{ backgroundColor: '#F11A7B', color: '#F9F5F6' }} className="search-button">
                <FontAwesomeIcon icon={faPaperPlane} />
              </button>
            </div>
          </div>
          <div className="social-media  ">
            <h4 style={{ color: '#3E001F'  }}> Follow Us  </h4>
            <p>   Follow us for skincare tips and more!</p>
            <div className="d-flex gap-3">
              <FontAwesomeIcon icon={faTwitter} color="#F11A7B" size="2x" />
              <FontAwesomeIcon icon={faFacebook} color="#F11A7B" size="2x" />
              <FontAwesomeIcon icon={faInstagram} color="#F11A7B" size="2x" />
              <FontAwesomeIcon icon={faYoutube} color="#F11A7B" size="2x" />
            </div>
          </div>
        </div>
      </div>

      <div style={{ backgroundColor: '#F11A7B', color: '#EEF7FF' }} className=" text-center "  >
        <p>
          © 2024 All rights reserved. Design by{" "}
          <span
            style={{ fontWeight: "bolder", color: '#EEF7FF' }}
            
            className="text-light text-weight-bold"
          >
            Theany
          </span>
        </p>
      </div>
    </div>
  );
};

export default Footer;
